using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame1
{
    public class Game
    {
        private Player Player1;
        private Player Player2;
        private int TurnCount = 0;
        Queue<Card> pool= new Queue<Card>();

        public Game(string player1name, string player2name)
        {
            Player1 = new Player(player1name);
            Player2 = new Player(player2name);

            //Console.WriteLine("Enter cards to be includedfrom 1 to _?");
            //int n = Convert.ToInt32(Console.ReadLine());
            int n = 3;
            var cards = DeckCreator.CreateCards(n); //Returns a shuffled set of cards
            
            DrawPile(cards); // Distribute cards among players

            Console.WriteLine("\n Game Begins \n");

            //Play until any of the player is out of cards or game is never ending(i.e .. More than 100 turn- end of game)
            while (!IsEndOfGame())
            {
                PlayTurn();
            }
        }

        //Check if there are cards in deck and discard pile
        public bool IsEndOfGame()
        {
            if (!Player1.DiscardPile.Any() && !Player2.DiscardPile.Any())
            {
                if (!Player1.Deck.Any() && !Player2.Deck.Any())
                {
                    Console.WriteLine("No cards at deck or discardpile to play further... Game Ends here.. Start ew game");
                    return true;
                }
            }
    
            //Check if  deck is empty -player 1
            if (!Player1.Deck.Any())
            {
                //Check if  DiscardPile is empty -player 1
                if (!Player1.DiscardPile.Any())
                {
                    Console.WriteLine("\n\n " +Player1.Name + " is out of cards!  " + Player2.Name + " WINS!");
                    return true;
                }
                else
                {
                    MoveDiscardPileToDeck(Player1);                
                }
            }
            //Check if  deck is empty -player 2
             if (!Player2.Deck.Any())
            {
                //Check if  DiscardPile is empty -player 2
                if (!Player2.DiscardPile.Any())
                {
                    Console.WriteLine("\n\n "+ Player2.Name + " is out of cards!  " + Player1.Name + " WINS!");
                    return true;
                }
                else
                {
                    MoveDiscardPileToDeck(Player2);
                }
            }
            else if (TurnCount > 100)
            {
                Console.WriteLine("Infinite game!  Let's call the whole thing off.");
                return true;
            }
            return false;
        }

        public void PlayTurn()
        {            
            // Each player flips a card
            var player1card = Player1.Deck.Dequeue();
            var player2card = Player2.Deck.Dequeue();

            pool.Enqueue(player1card);
            pool.Enqueue(player2card);

            Console.Write(Player1.Name + " plays :"
                              + player1card.DisplayName + "\n"
                              + Player2.Name + " plays :" + player2card.DisplayName );


            //  Task 3:If the cards show the same value, the winner of the next turn wins these cards as well .
            if (player1card.Value == player2card.Value)
            {
                Console.WriteLine("                       (====Thats a tie====)\n");
            }


            //Add the won cards to the winning player's deck, 
            //and display which player won that hand.
            if (player1card.Value < player2card.Value)
            {
                AddToWinnerDiscardPile(pool, Player2);                                            
            }
            else if (player2card.Value < player1card.Value)
            {
                AddToWinnerDiscardPile(pool, Player1);
            }

            TurnCount++;
         }

        //Shufflle discard pile and add back to deck
        private void MoveDiscardPileToDeck(Player player)
        {
            Queue<Card> shufledDiscardPlie;
            Console.WriteLine("\n\n ((" + player.Name + " : Deck is empty... Getting cards from discard pile...))");
            Console.WriteLine("discard pile before shuffle:");
            Print(player.DiscardPile);
            //shuffle the discard pile and add to deck 
            shufledDiscardPlie = DeckCreator.Shuffle(player.DiscardPile);
            AddDiscardPileToDeck(player, shufledDiscardPlie);
        }

        //add cards to the winner's Discardpile
        private void AddToWinnerDiscardPile(Queue<Card> pool, Player player)
        {
            Card tempCard;
            Console.WriteLine("                     Winner of this round is :   " + player.Name + "\n");
            while (pool.Any())
            {
                tempCard = pool.Dequeue();
                player.DiscardPile.Enqueue(tempCard);
            }
        }

        // Adding DiscardPile back to Deck
        private void AddDiscardPileToDeck(Player player, Queue<Card> shuffledDiscardPile)
        {
            Card tempCard;
            Console.WriteLine("\n\nDiscard pile moving to deck : " );
            while (shuffledDiscardPile.Any())
            {
                tempCard = shuffledDiscardPile.Dequeue();
                player.Deck.Enqueue(tempCard);
                Console.Write(tempCard.DisplayName + "  ");
            }
            //Cleaing DiscardPile
            player.DiscardPile.Clear();
            Console.Write("\n\n \n");
        }

        //draw pile
        private void DrawPile(Queue<Card> cards)
        {
            Card card;
            int cardCount = cards.Count;
            int cardsPerPlayer= cardCount/2;
            for (int i = 0; i < cardCount; i++)
            {
                card = cards.Dequeue();
                if (i < cardsPerPlayer)
                {
                    Player1.Deck.Enqueue(card);
                    Console.WriteLine("Player1 :" + card.DisplayName);
                }
                else
                {
                    Player2.Deck.Enqueue(card);
                    Console.WriteLine("Player2 :" + card.DisplayName);
                }
            }
        }

        //print cards
        private void Print(Queue<Card> cards)
        {          
            IEnumerator<Card> enumerator =
                        cards.GetEnumerator();

            while (enumerator.MoveNext())
            {

                Console.Write(enumerator.Current.DisplayName + "  ");
            }

        }
    }
}
